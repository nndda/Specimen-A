# Specimen A

### Mouse only controls
- Hold down <kbd>LMB</kbd>/<kbd>F</kbd> to move <br>
- Hold down <kbd>RMB</kbd>/<kbd>Space</kbd> and release to attack

---

## Building
The project can be built using Godot 4.3 official export templates.

---

## External Assets

### Fonts
- Silver (CC BY 4.0) by [Poppy Works](https://poppyworks.itch.io/)

### Sound Effects
- Universal UI/Menu Soundpack (CC BY 4.0) by [Ellr](https://ellr.itch.io/)

This project is licensed under [GNU GPLv3](LICENSE)

Copyright &copy; 2024 nnda
